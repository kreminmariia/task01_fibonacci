package Interval;

import java.util.Arrays;
import java.util.Scanner;

public class Interval {
    public static void welcoming(Scanner sc) {
        System.out.println("Hi again. You chose the Interval option. Now you need to type two int (min and max) boundary " +
                "values for your interval.");
        int min = sc.nextInt();
        int max = sc.nextInt();
        int[] array = getArray(min, max);
        int[] oddArray = filterArray(array, false);
        int[] evenArray = filterArray(array, true);
        System.out.println("Odd numbers from start to the end for your interval are: " + Arrays.toString(oddArray) +
                "\nAnd even numbers from the end to start are: " + Arrays.toString(evenArray) + "\nSum of odd = " +
                sumOfArrayElements(oddArray) + "\nSum of even = " + sumOfArrayElements(evenArray));
    }

    private static int[] getArray(int firstNumber, int lastNumber) {
        int[] usesArray = new int[lastNumber - firstNumber + 1];
        for (int i = firstNumber, j = 0; i < lastNumber + 1; i++, j++) {
            usesArray[j] = i;
        }
        return usesArray;
    }

    public static int[] filterArray(int[] fullArray, boolean mustBeEven){
        return Arrays.stream(fullArray).filter(num -> isEvenNum(num)==mustBeEven).toArray();
    }

    private static boolean isEvenNum(int num) {
        return num % 2 == 0;
    }

    private static int sumOfArrayElements(int[] array) {
        int sum = 0;
        for (int value : array) {
            sum += value;
        }
        return sum;
    }
}
