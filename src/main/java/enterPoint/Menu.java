package enterPoint;

import Fibonacci.Fibonacci;
import Interval.Interval;

import java.util.Scanner;

public class Menu {
    public static void main(String[] args) {
        System.out.println("Hi dear friend! You are able to perform some work with interval in this program or with " +
                "Fibonacci sequence. Please, select what are you choosing first?\n Press 1 of interval work. Press 2 " +
                "if Fibonacci work");
        Scanner sc = new Scanner(System.in);
        int userChoice = sc.nextInt();
        redirectChoice(userChoice, sc);
    }

    private static void redirectChoice(int selected, Scanner sc) {
        if (selected == 1) {
            Interval.welcoming(sc);
        } else if (selected == 2) {
            Fibonacci.welcoming(sc);
        } else {
            System.out.println("We do not have such option yet. Please choose 1 or 2. Thanks!");
            main(null);
        }
    }
}

