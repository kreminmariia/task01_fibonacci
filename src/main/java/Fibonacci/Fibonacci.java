package Fibonacci;

import Interval.Interval;

import java.util.Arrays;
import java.util.Scanner;

public class Fibonacci {
    public static void welcoming(Scanner sc) {
        System.out.println("Hi again. You chose the Fibonacci option. Now you need to type int number of sequence");
        int quantity = sc.nextInt();
        int[] fibonacciSequence = generateFib(quantity);
        int[] oddNumbersOnly = Interval.filterArray(fibonacciSequence, false);
        int[] evenNumbersOnly = Interval.filterArray(fibonacciSequence, true);
        System.out.println("Here is your Fibonacci sequence: " + Arrays.toString(fibonacciSequence) +
                "\nHere is the biggest odd number from sequence: " + getBiggestNumber(oddNumbersOnly) +
                "\nHere is the biggest even number from sequence: " + getBiggestNumber(evenNumbersOnly) +
                "\nHere is the percentage of odd number from sequence: " +
                getPercentageOfNumbers(oddNumbersOnly.length,fibonacciSequence.length) +
                "\nHere is the percentage of even number from sequence: " +
                getPercentageOfNumbers(evenNumbersOnly.length, fibonacciSequence.length));
    }

    private static int[] generateFib(int quantityOfAddition) {
        int[] arrayOfFib = new int[quantityOfAddition];
        for (int i = 2; i < quantityOfAddition; i++) {
            arrayOfFib[1] = 1;
            arrayOfFib[i] = arrayOfFib[i - 1] + arrayOfFib[i - 2];
        }
        return arrayOfFib;
    }

    private static int getBiggestNumber(int[] fibSeq) {
        int maximum = fibSeq[0];
        for (int i = 1; i < fibSeq.length; i++) {
            if (fibSeq[i] > maximum) {
                maximum = fibSeq[i];
            }
        }
        return maximum;
    }

    private static double getPercentageOfNumbers(double numerator, double denominator) {
        return (numerator / denominator)*100;
    }
}

